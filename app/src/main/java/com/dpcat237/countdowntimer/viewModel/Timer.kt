package com.dpcat237.countdowntimer.viewModel

import android.content.Context
import android.os.CountDownTimer
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dpcat237.countdowntimer.constants.NotificationConstants
import com.dpcat237.countdowntimer.manager.Notification
import com.dpcat237.countdowntimer.model.TimerState
import com.dpcat237.countdowntimer.model.TimerStatus
import com.dpcat237.countdowntimer.preference.TimerPreference
import java.util.concurrent.TimeUnit

class Timer : ViewModel() {
    private var timeStartInMilliSeconds = 0.toLong()
    private var blocked = false
    private lateinit var state: TimerState

    private val _currentTime = MutableLiveData<Long>()
    val currentTime: LiveData<Long>
        get() = _currentTime

    private val _currentTimeString = MutableLiveData<String>()
    val currentTimeString: LiveData<String>
        get() = _currentTimeString

    private val _currentPercentage = MutableLiveData<Int>()
    val currentPercentage: LiveData<Int>
        get() = _currentPercentage


    private var countDownTimer: CountDownTimer? = null

    fun backgroundDestroy(context: Context) {
        stopCountDownTimer()
        Notification.hideTimerNotification(context)
    }

    fun loadState(context: Context) {
        state = TimerPreference.getState(context)
        if (state.status == TimerStatus.STOPPED) {
            return
        }

        blocked = true
        if (state.status == TimerStatus.PAUSED) {
            timeStartInMilliSeconds = state.millisUntilFinished
            _currentTimeString.value = hmsTimeFormatter(state.millisUntilFinished)
            _currentTime.value = state.millisUntilFinished
            _currentPercentage.value = state.percentageLeft()
            blocked = false
            return
        }

        val pastCountedTime = timeStartInMilliSeconds - state.millisUntilFinished
        val pastDeviceTime = System.currentTimeMillis() - state.timeStartDeviceInMilliSeconds
        // is past more than counter because went to background
        timeStartInMilliSeconds = if (pastDeviceTime > pastCountedTime) {
            if (pastDeviceTime >= state.timeStartInMilliSeconds) {
                stopCountDownTimer()
                blocked = false
                return
            }
            state.timeStartInMilliSeconds - pastDeviceTime
        } else {
            state.millisUntilFinished
        }

        // state.status == TimerStatus.STARTED
        startCountDownTimer(context)
        blocked = false
    }

    fun pauseResumeTimer(context: Context) {
        if (blocked) {
            return
        }
        blocked = true

        if (state.millisUntilFinished < 1 || state.status == TimerStatus.STOPPED) {
            blocked = false
            return
        }

        if (state.status == TimerStatus.STARTED) {
            stopCountDownTimer()
            state.status = TimerStatus.PAUSED
            TimerPreference.saveState(context, state)
            //Notification.showTimerPaused(context)
            blocked = false
            return
        }

        stopCountDownTimer()
        timeStartInMilliSeconds = _currentTime.value?:0.toLong()
        state.status = TimerStatus.STARTED
        startCountDownTimer(context)
        blocked = false
    }

    fun startTimer(context: Context,hours: Int, minutes: Int) {
        if (blocked) {
            return
        }
        blocked = true

        state.status = TimerStatus.STOPPED
        stopCountDownTimer()
        cleanData(context)

        state.timeCountInMilliSeconds = if (hours>0 && minutes>0){
            TimeUnit.HOURS.toMillis(hours.toLong())+TimeUnit.MINUTES.toMillis(minutes.toLong())
        } else if(hours>0) {
            TimeUnit.HOURS.toMillis(hours.toLong())
        } else {
            TimeUnit.MINUTES.toMillis(minutes.toLong())
        }
        timeStartInMilliSeconds = state.timeCountInMilliSeconds

        state.status = TimerStatus.STARTED
        Notification.hideAlertNotification(context)
        startCountDownTimer(context)
        blocked = false
    }

    fun stopTimer(context: Context){
        if (blocked) {
            return
        }
        blocked = true

        if (state.status != TimerStatus.STARTED && state.status != TimerStatus.PAUSED) {
            blocked = false
            return
        }

        state.status = TimerStatus.STOPPED
        stopCountDownTimer()
        Notification.hideTimerNotification(context)
        state.timeCountInMilliSeconds = 0
        timeStartInMilliSeconds = 0
        _currentTimeString.value = hmsTimeFormatter(0)
        _currentPercentage.value = 100
        cleanData(context)
        blocked = false
    }

    private fun cleanData(context: Context) {
        state.cleanData()
        TimerPreference.saveState(context, state)
    }

    /**
     * method to convert millisecond to time format
     *
     * @param milliSeconds
     * @return HH:mm:ss time formatted string
     */
    private fun hmsTimeFormatter(milliSeconds: Long): String {
        return String.format(
            "%02d:%02d:%02d",
            TimeUnit.MILLISECONDS.toHours(milliSeconds),
            TimeUnit.MILLISECONDS.toMinutes(milliSeconds) - TimeUnit.HOURS.toMinutes(
                TimeUnit.MILLISECONDS.toHours(
                    milliSeconds
                )
            ),
            TimeUnit.MILLISECONDS.toSeconds(milliSeconds) - TimeUnit.MINUTES.toSeconds(
                TimeUnit.MILLISECONDS.toMinutes(
                    milliSeconds
                )
            )
        )
    }

    private fun reviewNotifications(context: Context, timeStr: String, percent: Int) {
        Notification.showTimerRunning(context,timeStr)
        if (percent < NotificationConstants.HALF_THRESHOLD && !state.notifiedHalf) {
            Notification.showTimerProcessHalf(context, timeStr)
            state.notifiedHalf = true
            TimerPreference.saveState(context, state)
            return
        }

        if (percent < NotificationConstants.WARNING_THRESHOLD && !state.notifiedWarning) {
            Notification.showTimerProcessWarning(context, timeStr)
            state.notifiedWarning = true
            TimerPreference.saveState(context, state)
            return
        }

        if (percent < NotificationConstants.ALERT_THRESHOLD && !state.notifiedAlert) {
            Notification.showTimerProcessAlert(context, timeStr)
            state.notifiedAlert = true
            TimerPreference.saveState(context, state)
            return
        }
    }

    private fun saveState(context: Context) {
        state.timeStartInMilliSeconds = timeStartInMilliSeconds
        TimerPreference.saveState(context, state)
    }

    /**
     * method to start count down timer
     */
    private fun startCountDownTimer(context: Context) {
        state.timeStartDeviceInMilliSeconds = System.currentTimeMillis()
        countDownTimer = object : CountDownTimer(timeStartInMilliSeconds, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                val timeStr = hmsTimeFormatter(millisUntilFinished)
                _currentTimeString.value = timeStr
                _currentTime.value = millisUntilFinished
                state.millisUntilFinished = millisUntilFinished
                val percent = state.percentageLeft()
                if (state.status != TimerStatus.PAUSED) {
                    _currentPercentage.value = percent
                }

                saveState(context)
                reviewNotifications(context, timeStr, percent)
            }

            override fun onFinish() {
                state.status = TimerStatus.STOPPED
                if (state.notifiedFinished) {
                    return
                }
                Notification.showTimerFinished(context)
                state.notifiedFinished = true
            }
        }.start()
    }

    /**
     * method to stop count down timer
     */
    private fun stopCountDownTimer() {
        if (countDownTimer != null) {
            countDownTimer!!.cancel()
        }
    }
}