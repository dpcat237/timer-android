package com.dpcat237.countdowntimer.constants

class NotificationConstants {
    companion object {
        const val ALERT_THRESHOLD = 10
        const val HALF_THRESHOLD = 50
        const val WARNING_THRESHOLD = 25
    }
}