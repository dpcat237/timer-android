package com.dpcat237.countdowntimer.manager

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import com.dpcat237.countdowntimer.R
import androidx.core.app.NotificationCompat
import com.dpcat237.countdowntimer.constants.TimerConstants
import com.dpcat237.countdowntimer.receiver.ActionReceiver
import com.dpcat237.countdowntimer.activity.MainActivity

class Notification {
    companion object {
        private const val CHANNEL_ID_TIMER = "timer_notification"
        private const val CHANNEL_ID_TIMER_STATUS = "timer_status_notification"
        private const val CHANNEL_NAME_TIMER = "Timer"
        private const val TIMER_ID = 0
        private const val TIMER_STATUS_ID = 1

        fun hideAlertNotification(context: Context) {
            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.cancel(TIMER_STATUS_ID)
        }

        fun hideTimerNotification(context: Context) {
            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.cancel(TIMER_ID)
        }

        fun showTimerFinished(context: Context) {
            val nBuilder = getBasicNotificationBuilder(context, CHANNEL_ID_TIMER_STATUS, true)
            nBuilder.setContentTitle("Time finished!")
                .setContentText("Time finished")

            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.createNotificationChannel(CHANNEL_ID_TIMER_STATUS, CHANNEL_NAME_TIMER, false)

            nManager.notify(TIMER_STATUS_ID, nBuilder.build())
        }

        fun showTimerPaused(context: Context){
            //println("tut: showTimerPaused")
            /*
            val resumeIntent = Intent(context, TimerNotificationActionReceiver::class.java)
            resumeIntent.action = AppConstants.ACTION_RESUME
            val resumePendingIntent = PendingIntent.getBroadcast(context,
                    0, resumeIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val nBuilder = getBasicNotificationBuilder(context, CHANNEL_ID_TIMER, true)
            nBuilder.setContentTitle("Timer is paused.")
                    .setContentText("Resume?")
                    .setContentIntent(getPendingIntentWithStack(context, TimerActivity::class.java))
                    .setOngoing(true)
                    .addAction(R.drawable.ic_play_arrow, "Resume", resumePendingIntent)

            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.createNotificationChannel(CHANNEL_ID_TIMER, CHANNEL_NAME_TIMER, true)

            nManager.notify(TIMER_ID, nBuilder.build())
             */
        }

        fun showTimerProcessAlert(context: Context, timeStr: String) {
            val nBuilder = getBasicNotificationBuilder(context, CHANNEL_ID_TIMER_STATUS, true)
            nBuilder.setContentTitle("A few time left")
                .setContentText("Is left only: $timeStr")

            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.createNotificationChannel(CHANNEL_ID_TIMER_STATUS, CHANNEL_NAME_TIMER, true)

            nManager.notify(TIMER_STATUS_ID, nBuilder.build())
        }

        fun showTimerProcessHalf(context: Context, timeStr: String) {
            val nBuilder = getBasicNotificationBuilder(context, CHANNEL_ID_TIMER_STATUS, true)
            nBuilder.setContentTitle("Half time past")
                .setContentText("Is left half of the time: $timeStr")

            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.createNotificationChannel(CHANNEL_ID_TIMER_STATUS, CHANNEL_NAME_TIMER, true)

            nManager.notify(TIMER_STATUS_ID, nBuilder.build())
        }

        fun showTimerProcessWarning(context: Context, timeStr: String) {
            val nBuilder = getBasicNotificationBuilder(context, CHANNEL_ID_TIMER_STATUS, true)
            nBuilder.setContentTitle("Less than half time left")
                .setContentText("Left of the time: $timeStr")

            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.createNotificationChannel(CHANNEL_ID_TIMER_STATUS, CHANNEL_NAME_TIMER, true)

            nManager.notify(TIMER_STATUS_ID, nBuilder.build())
        }

        fun showTimerRunning(context: Context,timeStr: String){
            /* doesn't work intent
            val pauseIntent = Intent(context, ActionReceiver::class.java)
            pauseIntent.action = TimerConstants.ACTION_PAUSE
            val pausePendingIntent = PendingIntent.getBroadcast(context,
                0, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT)*/

            val nBuilder = getBasicNotificationBuilder(context, CHANNEL_ID_TIMER, false)
            nBuilder.setContentTitle("Timer is Running.")
                .setContentText("End: $timeStr")
                .setContentIntent(getPendingIntentWithStack(context, MainActivity::class.java))
                .setOngoing(true)
                //.addAction(R.drawable.ic_pause, "Pause", pausePendingIntent)

            val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            nManager.createNotificationChannel(CHANNEL_ID_TIMER, CHANNEL_NAME_TIMER, false)

            nManager.notify(TIMER_ID, nBuilder.build())
        }

        private fun getBasicNotificationBuilder(context: Context, channelId: String, playSound: Boolean)
                : NotificationCompat.Builder{
            val notificationSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val nBuilder = NotificationCompat.Builder(context, channelId)
                .setSmallIcon(R.drawable.ic_timer)
                .setAutoCancel(true)
                .setDefaults(0)
            if (playSound) nBuilder.setSound(notificationSound)
            return nBuilder
        }

        private fun <T> getPendingIntentWithStack(context: Context, javaClass: Class<T>): PendingIntent {
            val resultIntent = Intent(context, javaClass)
            resultIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

            val stackBuilder = TaskStackBuilder.create(context)
            stackBuilder.addParentStack(javaClass)
            stackBuilder.addNextIntent(resultIntent)

            val pendingIntent = if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.S){
                stackBuilder.getPendingIntent(1, PendingIntent.FLAG_MUTABLE);

            } else {
                stackBuilder.getPendingIntent(1, PendingIntent.FLAG_UPDATE_CURRENT);

            }
            return pendingIntent
        }

        @TargetApi(26)
        private fun NotificationManager.createNotificationChannel(channelID: String,
                                                                  channelName: String,
                                                                  playSound: Boolean){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                val channelImportance = if (playSound) NotificationManager.IMPORTANCE_DEFAULT
                else NotificationManager.IMPORTANCE_LOW
                val nChannel = NotificationChannel(channelID, channelName, channelImportance)
                nChannel.enableLights(true)
                nChannel.lightColor = Color.BLUE
                this.createNotificationChannel(nChannel)
            }
        }
    }
}