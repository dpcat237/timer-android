package com.dpcat237.countdowntimer.preference

import android.content.Context
import com.dpcat237.countdowntimer.model.TimerState
import com.dpcat237.countdowntimer.model.TimerStatus

const val PREF_TIMER_STATE = "pref_timer_state"
const val PREF_TIME_COUNT = "time_count"
const val PREF_TIME_START = "time_start"
const val PREF_TIME_DEVICE_START = "time_device_start"
const val PREF_TIME_UNIT_FINISHED = "time_until_finished"
const val PREF_NOTIFIED_ALERT = "notified_alert"
const val PREF_NOTIFIED_FINISHED = "notified_finished"
const val PREF_NOTIFIED_HALF = "notified_half"
const val PREF_NOTIFIED_WARNING = "notified_warning"
const val PREF_STATUS = "status"

class TimerPreference {
    companion object {
        fun getState(context: Context): TimerState{
            val st = TimerState()
            val sharedPref = context.getSharedPreferences(PREF_TIMER_STATE,Context.MODE_PRIVATE)
            st.timeCountInMilliSeconds = sharedPref.getLong(PREF_TIME_COUNT, 0)
            st.timeStartInMilliSeconds = sharedPref.getLong(PREF_TIME_START, 0)
            st.timeStartDeviceInMilliSeconds = sharedPref.getLong(PREF_TIME_DEVICE_START, 0)
            st.millisUntilFinished = sharedPref.getLong(PREF_TIME_UNIT_FINISHED, 0)
            st.notifiedAlert = sharedPref.getBoolean(PREF_NOTIFIED_ALERT, false)
            st.notifiedFinished = sharedPref.getBoolean(PREF_NOTIFIED_FINISHED, false)
            st.notifiedHalf = sharedPref.getBoolean(PREF_NOTIFIED_HALF, false)
            st.notifiedWarning = sharedPref.getBoolean(PREF_NOTIFIED_WARNING, false)
            val statusStr = sharedPref.getString(PREF_STATUS, TimerStatus.STOPPED.toString())
            st.status = TimerStatus.valueOf(statusStr!!)

            return st
        }

        fun saveState(context: Context, st: TimerState) {
            val sharedPref = context.getSharedPreferences(PREF_TIMER_STATE,Context.MODE_PRIVATE)
            sharedPref.edit().
            putLong(PREF_TIME_COUNT, st.timeCountInMilliSeconds).
            putLong(PREF_TIME_START, st.timeStartInMilliSeconds).
            putLong(PREF_TIME_DEVICE_START, st.timeStartDeviceInMilliSeconds).
            putLong(PREF_TIME_UNIT_FINISHED, st.millisUntilFinished).
            putBoolean(PREF_NOTIFIED_ALERT, st.notifiedAlert).
            putBoolean(PREF_NOTIFIED_FINISHED, st.notifiedFinished).
            putBoolean(PREF_NOTIFIED_HALF, st.notifiedHalf).
            putBoolean(PREF_NOTIFIED_WARNING, st.notifiedWarning).
            putString(PREF_STATUS, st.status.toString()).
            apply()
        }
    }
}