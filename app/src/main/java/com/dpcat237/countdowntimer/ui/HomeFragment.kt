package com.dpcat237.countdowntimer.ui

import android.graphics.Color
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.dpcat237.countdowntimer.constants.NotificationConstants
import com.dpcat237.countdowntimer.viewModel.Timer
import com.dpcat237.countdowntimer.databinding.HomeFragmentBinding

class HomeFragment : Fragment() {
    internal var view: View? = null
    private var _binding: HomeFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var viewModel: Timer

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this)[Timer::class.java]
        viewModel.loadState(requireContext())

        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.view = view

        // View observe
        binding.btnSet.setOnClickListener {
            if (binding.layoutSet.visibility == View.VISIBLE) {
                binding.layoutSet.visibility = View.GONE
                return@setOnClickListener
            }
            binding.layoutSet.visibility = View.VISIBLE
        }

        binding.btnStart.setOnClickListener {
            if (binding.inputHours.text.toString() == "" && binding.inputMinutes.text.toString() == "") {
                return@setOnClickListener
            }

            var hours = "0"
            if (binding.inputHours.text.toString() != "") {
                hours = binding.inputHours.text.toString()
            }
            var minutes = "0"
            if (binding.inputMinutes.text.toString() != "") {
                minutes = binding.inputMinutes.text.toString()
            }

            binding.layoutSet.visibility = View.GONE
            viewModel.startTimer(
                requireContext(),
                hours.toInt(),
                minutes.toInt()
            )
            binding.inputHours.text.clear()
            binding.inputMinutes.text.clear()
        }
        binding.btnPause.setOnClickListener {
            viewModel.pauseResumeTimer(requireContext())
        }
        binding.btnStop.setOnClickListener {
            viewModel.stopTimer(requireContext())
        }

        // Model observe
        viewModel.currentTimeString.observe(viewLifecycleOwner) {
            binding.textViewTime.text = it
        }
        viewModel.currentPercentage.observe(viewLifecycleOwner) {
            binding.progressBarCircle.progress = it
            if (binding.progressBarCircle.progress == 0 && it.toInt() > 1) {
                binding.progressBarCircle.max = 100
            } else {
                binding.layoutSet.visibility = View.GONE
            }
            setProgressBarColor(it)
        }
    }

    override fun onDestroy() {
        viewModel.backgroundDestroy(requireContext())
        super.onDestroy()
    }

    private fun setProgressBarColor(percent: Int) {
        when {
            percent <= NotificationConstants.ALERT_THRESHOLD -> {
                binding.progressBarCircle.progressTintList = ColorStateList.valueOf(Color.argb(255, 244, 67, 54))
            }
            percent <= NotificationConstants.WARNING_THRESHOLD -> {
                binding.progressBarCircle.progressTintList = ColorStateList.valueOf(Color.argb(255, 255, 152, 0))
            }
            else -> {
                binding.progressBarCircle.progressTintList = ColorStateList.valueOf(Color.argb(255, 76, 175, 80))
            }
        }
    }
}