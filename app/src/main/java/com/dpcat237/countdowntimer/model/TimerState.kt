package com.dpcat237.countdowntimer.model

enum class TimerStatus {
    STARTED, PAUSED, STOPPED
}

class TimerState {
    var timeCountInMilliSeconds = 0.toLong()
    var timeStartInMilliSeconds = 0.toLong()
    var timeStartDeviceInMilliSeconds = 0.toLong()
    var millisUntilFinished = 0.toLong()
    var notifiedAlert = false
    var notifiedFinished = false
    var notifiedHalf = false
    var notifiedWarning = false
    lateinit var status: TimerStatus

    fun cleanData(){
        timeCountInMilliSeconds = 0.toLong()
        timeStartInMilliSeconds = 0.toLong()
        timeStartDeviceInMilliSeconds = 0.toLong()
        millisUntilFinished = 0.toLong()
        notifiedAlert = false
        notifiedFinished = false
        notifiedHalf = false
        notifiedWarning = false
        status = TimerStatus.STOPPED
    }

    fun percentageLeft(): Int{
        return ((millisUntilFinished.toFloat() / timeCountInMilliSeconds.toFloat()) *100).toInt()
    }
}