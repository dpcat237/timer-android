package com.dpcat237.countdowntimer.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.dpcat237.countdowntimer.constants.TimerConstants
import com.dpcat237.countdowntimer.viewModel.Timer

class ActionReceiver  : BroadcastReceiver(){
    private lateinit var viewModel: Timer

    override fun onReceive(context: Context, intent: Intent) {
        println("tut: ActionReceiver onReceive")
        println("tut: ActionReceiver action"+intent.action)
        when (intent.action) {
            TimerConstants.ACTION_STOP -> {
                println("tut: ACTION_STOP")
                //TimerActivity.removeAlarm(context)
                //PrefUtil.setTimerState(TimerActivity.TimerState.Stopped, context)
                //NotificationUtil.hideTimerNotification(context)
            }
            TimerConstants.ACTION_PAUSE -> {
                println("tut: ACTION_PAUSE1")
                viewModel.pauseResumeTimer(context)
                println("tut: ACTION_PAUSE2")
                /*var secondsRemaining = PrefUtil.getSecondsRemaining(context)
                val alarmSetTime = PrefUtil.getAlarmSetTime(context)
                val nowSeconds = TimerActivity.nowSeconds

                secondsRemaining -= nowSeconds - alarmSetTime
                PrefUtil.setSecondsRemaining(secondsRemaining, context)

                TimerActivity.removeAlarm(context)
                PrefUtil.setTimerState(TimerActivity.TimerState.Paused, context)
                NotificationUtil.showTimerPaused(context)*/
            }
            TimerConstants.ACTION_RESUME -> {
                println("tut: ACTION_RESUME")
                /*val secondsRemaining = PrefUtil.getSecondsRemaining(context)
                val wakeUpTime = TimerActivity.setAlarm(context, TimerActivity.nowSeconds, secondsRemaining)
                PrefUtil.setTimerState(TimerActivity.TimerState.Running, context)
                NotificationUtil.showTimerRunning(context, wakeUpTime)*/
            }
            TimerConstants.ACTION_START -> {
                println("tut: ACTION_START")
                /*val minutesRemaining = PrefUtil.getTimerLength(context)
                val secondsRemaining = minutesRemaining * 60L
                val wakeUpTime = TimerActivity.setAlarm(context, TimerActivity.nowSeconds, secondsRemaining)
                PrefUtil.setTimerState(TimerActivity.TimerState.Running, context)
                PrefUtil.setSecondsRemaining(secondsRemaining, context)
                NotificationUtil.showTimerRunning(context, wakeUpTime)*/
            }
        }
    }
}